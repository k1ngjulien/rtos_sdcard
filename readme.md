# Getting an SD Card Working on the STM32

The following is a documentation on how I got a uSD-Card working on an STM32F411VE Discovery Board under FreeRTOS. The SD card is connected via an [Adafruit SD Card Module](https://www.adafruit.com/product/4682) to the STM32 using the SDIO protocol and peripheral inside the chip.

For reference, here is the documentation of the FAT32 library: <http://elm-chan.org/fsw/ff/>

> __TL;DR:__ 
> 
> Configure `FreeRTOS`.
> 
> Configure `SDIO` with 
> - `SD 1 Bit`
> - `SDIO Global Interrupt`
> - Requests on DMA2 for `SDIO_RX` and `SDIO_TX`
>
> Configure `FAT32` and set the GPIO for Card Detect.
> If needed, invert Card Detect in software by overriding the implementation.

## Setup in CubeMX/CubeIDE

I started by configuring both the SDIO peripheral and the FAT32 middleware.

### SDIO peripheral

The SD Module is connected to the Discovery Board using a 1 Bit SDIO interface.
It was wired up to the Pins specified in the GPIO config.

### FAT32 middleware

This was enabled and PE2 set as the GPIO input for the card detect.


## Example from the docs

Google lead me to [a user manual](https://www.st.com/resource/en/user_manual/um1721-developing-applications-on-stm32cube-with-fatfs-stmicroelectronics.pdf) by ST with an example, which was broken...

I had to fix a stray semicolon and an off by 1 error 😬.


## Invert Card Detect in Driver:

The STM32s FAT32 Driver and SDIO Driver expect the Card Detect pin to be active low. The SD module provides an active high signal, meaning it is at 3.3V when a card is inserted.
We could solve this in hardware, but changing it in software works too and saves one FET and one pull-up resistor.

The following code has to be pasted _somewhere_ in the project. Doesn't really matter where it goes.
It is copied from the original implementation with the 0x0 changed to 0x1, to change to active high.

```c
uint8_t BSP_SD_IsDetected(void)  
{  
  __IO uint8_t status = SD_PRESENT;  
  
  if (BSP_PlatformIsDetected() == 0x1)  
  {
    status = SD_NOT_PRESENT;  
  }  
  
  return status;  
}
```

As the original is defined with `__weak` linkage, we can just provide a function with the same name and the linker will override the original with our implementation.
This got me a first successful mount.

## Enable DMA

Next I had to enable the correct DMA Streams. This is a requirement when using the FAT32 under FreeRTOS to keep the scheduler in charge.

Luckily if you have both FAT32 and FreeRTOS enabled, `SDIO_RX` and `SDIO_TX` can be selected when clicking "Add". I have not yet tested what the plain `SDIO` request does.

![DMA Setup](doc/dma.png)

That got me a successful `f_open`.


At this point noticed that my laptop didn't recognize the SD Card anymore, which was troubling…

![dmesg errors](doc/dmesg.png)

`dmesg` complained for a bit when I put the card back into my laptop. probably some corruption from improperly unmounting the card during testing.
Fortunately, running through the whole mount, open, close and unmount sequence in the debugger, powering off the STM32 and then ejecting the SD card from the module seems to have fixed this.

The file was created by `f_open` but no contents were written…

Open seems to work, but we crash with `FR_DISK_ERROR` on write…
Increasing stack size of the task did not help.

## Enable SDIO Global Interrupt

Looks like that interrupt writes to a queue telling RTOS that the writing has completed. So I had to enable that as well.
This finally fixed the writes!

![Interrupt enabled](doc/sdio_interrupt.png)

# Example Write Function

For the rest of the code, check out [`Core/Src/freertos.c`](https://gitlab.com/k1ngjulien/rtos_sdcard/-/blob/main/Core/Src/freertos.c)

```c
FRESULT sd_write(char *filepath, const BYTE *data, size_t len, uint8_t mode) {  
  FIL myFile;  
  FRESULT res;  
  UINT bytesWritten = 0;  
  
  // always open with write, caller can decide if create or append  
  res = f_open(&myFile, filepath, mode | FA_WRITE);  
  if (res != FR_OK) {  
    return res;  
  }  
  
  res = f_write(&myFile, data, len, (UINT*)bytesWritten);  
  if (res != FR_OK) {  
    return res;  
  }  
  
  return f_close(&myFile);  
}
```

This took a bit of experimenting and playing around since there's not much documentation out there,
but its actually not too difficult to set up once you know what to do :).

Hope this could help!

Cheers,
Julian

# Development Setup

To be able to modify and run the code, first clone or download and extract the repository from gitlab.

Open the `rtos_sdcard.ioc` project file in STM32CubeMX/IDE or the full folder when using CLion (with CubeMX and OpenOCD installed) or VSCode (with Extensions)

Then run the __Code Generation__ at least once to load all the required libraries.

From then on you should be able to build and flash the code to a microcontroller.