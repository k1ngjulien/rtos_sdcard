/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "fatfs.h"
#include <string.h>


/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define BUFLEN 4069
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 1024 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

FRESULT fs_init();
FRESULT fs_deInit();

FRESULT sd_write(char *filepath, const BYTE *data, size_t len, uint8_t mode);
FRESULT sd_read_file(char* path, BYTE* buffer, size_t bufferSize, size_t*bytesReadTotal);

FRESULT copy_file(char* from, char* to);


/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
  // THIS FUNCTION RUNS BEFORE KERNEL START
  // YOU CANNOT YET USE FREERTOS FUNCTIONS HERE
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  char *data = "write this shit to the card!\n";
  char *path = "/STM32.TXT";

  // start up the file system and mount sd card
  if(fs_init() == FR_OK) {
    // append data to file or create it
    sd_write(path, (BYTE *) data, strlen(data), FA_OPEN_APPEND);

    copy_file("/file1.txt", "/copy.txt");


    // shut down file system unmount sd card, so it can be safely removed
    fs_deInit();
  }

  /* Infinite loop */
  for(;;)
  {
    // do nothing for now
    osDelay(100);
  }
  /* USER CODE END StartDefaultTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
/**
 * write data to a file. will automatically open, write and close the file.
 * @param filepath full file path with name and extension
 * @param data array containing data to write
 * @param len number of data bytes to write
 * @param mode allows choosing between creating a new file or appending to an existing one. write is always set.
 * @return
 */
FRESULT sd_write(char *filepath, const BYTE *data, size_t len, uint8_t mode) {
  FIL file;
  FRESULT res;
  UINT bytesWritten = 0;

  // always open with write, caller can decide if create or append
  res = f_open(&file, filepath, mode | FA_WRITE);
  if (res) return res;


  res = f_write(&file, data, len, (UINT*)bytesWritten);
  if (res) {
    f_close(&file);
    return res;
  }

  return f_close(&file);
}

FRESULT sd_read_file(char* path, BYTE* buffer, size_t bufferSize, size_t* bytesReadTotal) {
  FRESULT res;
  FIL infile;
  UINT bytesRead;
  *bytesReadTotal = 0;

  res = f_open(&infile, path, FA_READ);
  if (res) return res;

  // read full file into buffer
  while ((*bytesReadTotal < bufferSize) &&
         ((res = f_read(&infile, buffer, BLOCKSIZE, &bytesRead)) == FR_OK) &&
         (bytesRead > 0)) {
    *bytesReadTotal += bytesRead;
  }

  if (res) {
    // something went wrong while reading
    // try closing the file anyway and return why writing failed
    f_close(&infile);
    return res;
  }

  return f_close(&infile);
}


// create buffer here, so we don't need as much stack space in the task
BYTE buffer[BUFLEN];

FRESULT copy_file(char* from, char* to) {
  FRESULT res;
  UINT bytesReadTotal;

  res = sd_read_file(from, buffer, BUFLEN, &bytesReadTotal);
  if (res) return res;

  return sd_write(to, buffer, bytesReadTotal, FA_OPEN_APPEND);
}

FRESULT fs_init() {
  // immediately mount file system
  return f_mount(&SDFatFS, SDPath, 1);
}

FRESULT fs_deInit() {
  // mount with null is an unmount
  return f_mount(NULL, SDPath, 1);
}

// override card detect implementation to change from active low to active high,
// as provided by the adafruit sd card module
// could be done by modifying the source but this copy paste override seemed easier
uint8_t BSP_SD_IsDetected(void)
{
  __IO uint8_t status = SD_PRESENT;

  if (BSP_PlatformIsDetected() == 0x1)
  {
    status = SD_NOT_PRESENT;
  }

  return status;
}

/* USER CODE END Application */

